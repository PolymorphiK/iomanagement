﻿namespace IOManagement.Example {
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.Audio;

	public class UISettings : MonoBehaviour {
		public Toggle musicSwitch;
		public Slider musicVolume;

		public Toggle sfxSwitch;
		public Slider sfxVolume;

		public AudioMixerGroup musicMixer;
		public AudioMixerGroup sfxMixer;

		//[HideInInspector]
		public OptionsData optionsData;

		private void Start() {
			var iLoadResult = IOModule.Load("Options Data", Method<OptionsData>.Xml, Application.dataPath, "Data");

			if(!iLoadResult.Success) {
				Debug.Log(iLoadResult.Error);

				// The file may not exist so create it.
				this.optionsData = new OptionsData();

				var iSaveResult = IOModule.Save(this.optionsData, "Options Data", Method<OptionsData>.Xml, Application.dataPath, "Data");

				if(!iSaveResult.Success) {
					Debug.LogError(iSaveResult.Error);
				} else {
					Debug.Log(iSaveResult.Message);
				}
			} else {
				this.optionsData = iLoadResult.Payload;

				Debug.Log(iLoadResult.Message);
			}

			this.musicSwitch.isOn = this.optionsData.IsMusicEnabled;
			this.musicVolume.value = this.optionsData.MusicVolume;
			this.musicVolume.interactable = this.optionsData.IsMusicEnabled;

			if(this.optionsData.IsMusicEnabled) {
				var volume = Mathf.Lerp(-80.0F, 0.0F, this.optionsData.MusicVolume / 100.0F);

				this.musicMixer.audioMixer.SetFloat("musicVolume", volume);
			} else {
				this.musicMixer.audioMixer.SetFloat("musicVolume", -80.0F);
			}

			this.sfxSwitch.isOn = this.optionsData.IsSFXEnabled;
			this.sfxVolume.value = this.optionsData.SFXVolume;
			this.sfxVolume.interactable = this.optionsData.IsSFXEnabled;

			if(this.optionsData.IsSFXEnabled) {
				var volume = Mathf.Lerp(-80.0F, 0.0F, this.optionsData.SFXVolume / 100.0F);

				this.sfxMixer.audioMixer.SetFloat("sfxVolume", volume);
			} else {
				this.sfxMixer.audioMixer.SetFloat("sfxVolume", -80.0F);
			}
		}

		public void OnMusicEnabledChanged(bool value) {
			this.optionsData.IsMusicEnabled = value;

			this.musicVolume.interactable = this.optionsData.IsMusicEnabled;

			if(this.optionsData.IsMusicEnabled) {
				var volume = Mathf.Lerp(-80.0F, 0.0F, this.optionsData.MusicVolume / 100.0F);

				this.musicMixer.audioMixer.SetFloat("musicVolume", volume);
			} else {
				this.musicMixer.audioMixer.SetFloat("musicVolume", -80.0F);
			}
		}

		public void OnMusicVolumeChanged(float value) {
			this.optionsData.MusicVolume = value;

			var volume = Mathf.Lerp(-80.0F, 0.0F, this.optionsData.MusicVolume / 100.0F);

			this.musicMixer.audioMixer.SetFloat("musicVolume", volume);
		}

		public void OnSFXEnabledChanged(bool value) {
			this.optionsData.IsSFXEnabled = value;

			this.sfxVolume.interactable = this.optionsData.IsSFXEnabled;

			if(this.optionsData.IsSFXEnabled) {
				var volume = Mathf.Lerp(-80.0F, 0.0F, this.optionsData.SFXVolume / 100.0F);

				this.sfxMixer.audioMixer.SetFloat("sfxVolume", volume);
			} else {
				this.sfxMixer.audioMixer.SetFloat("sfxVolume", -80.0F);
			}
		}

		public void OnSFXVolumeChanged(float value) {
			this.optionsData.SFXVolume = value;

			var volume = Mathf.Lerp(-80.0F, 0.0F, this.optionsData.SFXVolume / 100.0F);

			this.sfxMixer.audioMixer.SetFloat("sfxVolume", volume);
		}

		public void Save() {
			var iSaveResult = IOModule.Save<OptionsData>(this.optionsData, "Options Data", Method<OptionsData>.Xml, Application.dataPath, "Data");

			if(iSaveResult.Success) {
				Debug.Log(iSaveResult.Message);
			} else {
				Debug.LogError(iSaveResult.Error);
			}
		}
	}
}