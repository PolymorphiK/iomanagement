﻿namespace IOManagement.Example {
	[System.Serializable]
	public class OptionsData {
		private float musicVolume;
		private float sfxVolume;

		public bool IsMusicEnabled { get; set; }

		public float MusicVolume {
			get {
				return this.musicVolume;
			}
			set {
				value = value < 0.0F ? 0.0F : value;
				value = value > 100.0F ? 100.0F : value;

				this.musicVolume = value;
			}
		}

		public bool IsSFXEnabled { get; set; }

		public float SFXVolume {
			get {
				return this.sfxVolume;
			}
			set {
				value = value < 0.0F ? 0.0F : value;
				value = value > 100.0F ? 100.0F : value;

				this.sfxVolume = value;
			}
		}

		public OptionsData() {
			this.IsMusicEnabled = true;
			this.MusicVolume = 100.0F;

			this.IsSFXEnabled = true;
			this.SFXVolume = 100.0F;
		}
	}
}