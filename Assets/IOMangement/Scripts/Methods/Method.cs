﻿#region License
// Copyright (c) 2019 Kevin Pacheco
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// Contact: k.pacheco@me.com subject - IOManagement
#endregion

namespace IOManagement {
	using System.IO;

	/// <summary>
	/// Serialization/Deserialization container class.
	/// </summary>
	/// <typeparam name="T">Type to be Serialized/Deserialized.</typeparam>
	public partial class Method<T> {
		public delegate void Serializer(Stream stream, T instance);
		public delegate T Deserializer(Stream stream);

		public readonly string extension;
		public readonly Serializer serializer;
		public readonly Deserializer deserializer;

		/// <summary>
		/// Constructor to create a Serialization/Deserialization method.
		/// </summary>
		/// <param name="serializer">Serializer (Stream, T).</param>
		/// <param name="deserializer">Deserializer (Stream).</param>
		/// <param name="extension">Extension for the file.</param>
		public Method(Serializer serializer, Deserializer deserializer, string extension) {
			this.serializer = serializer;
			this.deserializer = deserializer;
			this.extension = extension;
		}

		/// <summary>
		/// Builder for creating a Serialization/Deserialization method.
		/// </summary>
		public class Builder {
			private string extension;
			private Serializer serializer;
			private Deserializer deserializer;

			public Builder(string extension) {
				this.extension = extension;
			}

			public Builder SetSerializer(Serializer serializer) {
				this.serializer = serializer;

				return this;
			}

			public Builder SetDeserializer(Deserializer deserializer) {
				this.deserializer = deserializer;

				return this;
			}

			public Method<T> Build() {
				return new Method<T>(this.serializer, this.deserializer, this.extension);
			}
		}
	}
}