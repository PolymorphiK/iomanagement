﻿#region License
// Copyright (c) 2019 Kevin Pacheco
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// Contact: k.pacheco@me.com subject - IOManagement
#endregion

namespace IOManagement {
	using System.IO;

	public partial class IOModule {
		public static bool CREATE_BACKUP = true;

		public static IResult Save<T>(T instance, string name, Method<T> method, string location = null, string subfolder = null) {
			var result = new Result();

			try {
				var directory = string.Empty;

				if(string.IsNullOrEmpty(location)) {
					location = UnityEngine.Application.persistentDataPath;
				}

				if(string.IsNullOrEmpty(subfolder)) {
					directory = location;
				} else {
					directory = Path.Combine(location, subfolder);
				}

				if(!Directory.Exists(directory)) {
					Directory.CreateDirectory(directory);
				}

				var fileName = string.Format(
					"{0}.{1}",
					name,
					method.extension);

				var path = Path.Combine(directory, fileName);

				var temp = Path.Combine(directory, fileName + ".temp");

				using(var stream = new FileStream(temp, FileMode.Create)) {
					method.serializer(stream, instance);
				}

				if(IOModule.CREATE_BACKUP) {
					if(File.Exists(path)) {
						var backup = path + ".backup";

						File.Delete(backup);
						File.Move(path, backup);
					}
				}

				File.Delete(path);
				File.Move(temp, path);

				result.Message = name + " saved!";
			} catch(System.Exception e) {
				result.Error = e.Message;
			}

			return result;
		}
	}
}