﻿#region License
// Copyright (c) 2019 Kevin Pacheco
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// Contact: k.pacheco@me.com subject - IOManagement
#endregion

namespace IOManagement {
	using System.Text;

	public static partial class IOModule {
		public static Encoding encoding = Encoding.GetEncoding("UTF-8");
		public static readonly string Location = UnityEngine.Application.persistentDataPath;

		/// <summary>
		/// I/O Operation result.
		/// </summary>
		private class Result : IResult {
			public string Message { get; set; }

			public string Error { get; set; }

			public bool Success {
				get {
					return string.IsNullOrEmpty(this.Error);
				}
			}
		}

		/// <summary>
		/// I/O Operation result with the loaded payload.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		private class Result<T> : Result, IResult<T> {
			public T Payload { get; set; }
		}
	}
}