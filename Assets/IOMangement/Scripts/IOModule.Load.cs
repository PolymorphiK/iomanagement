﻿#region License
// Copyright (c) 2019 Kevin Pacheco
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// Contact: k.pacheco@me.com subject - IOManagement
#endregion

namespace IOManagement {
	using System.IO;

	public partial class IOModule {
		public static bool ATTEMPT_SAVE_REPAIR = true;

		public static IResult<T> Load<T>(string name, Method<T> method, string location = null, string subfolder = null) {
			var result = new Result<T>();

			try {
				var directory = string.Empty;

				if(string.IsNullOrEmpty(location)) {
					location = UnityEngine.Application.persistentDataPath;
				}

				if(string.IsNullOrEmpty(subfolder)) {
					directory = location;
				} else {
					directory = Path.Combine(location, subfolder);
				}

				if(!Directory.Exists(directory)) {
					Directory.CreateDirectory(directory);

					throw new IOException(
						string.Format(
							"Directory {0} does not exist!",
							directory));
				}

				var fileName = string.Format(
					"{0}.{1}",
					name,
					method.extension);

				var path = Path.Combine(directory, fileName);

				var message = string.Empty;

				if(IOModule.ATTEMPT_SAVE_REPAIR) {
					var temp = path + ".temp";

					if(File.Exists(temp)) {
						// Something went wrong during save!
						// Attempting to load from backup.
						message += "Loading from a backfile!\t";

						var backup = path + ".backup";

						if(File.Exists(backup)) {
							// we found a backup attempting to use this

							File.Delete(path);
							File.Move(backup, path);
						}

						File.Delete(temp);
					}
				}

				using(var stream = new FileStream(path, FileMode.Open)) {
					result.Payload = method.deserializer(stream);
				}

				message += name + " loaded!";

				result.Message = message;
			} catch(System.Exception e) {
				result.Error = e.Message;
			}

			return result;
		}
	}
}